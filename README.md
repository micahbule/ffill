# Liff API

This is the repository for the Liff API. This RESTful API is built with [NestJS](https://nestjs.com/).

## Pre-requisites

- Node v18 or higher
- PostgreSQL

## Installation

```bash
$ npm install
```

1. Create a DB in Postgres with the name `liff`.

## Setup

Notes: This repository is equipped with [migrations](https://mikro-orm.io/docs/migrations) for schema/table creation.

1. Create a `.env` file using the `env.sample` file contents. **Make sure that it is .env** and not any other `.env` variations as MikroORM is pretty picky.
2. If this is the first time you're executing this API, run `npm run db:init` to generate the schema in the database.
3. If you already have an existing database and want to update the schema/tables, run `npm run db:migrate-latest`.

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
