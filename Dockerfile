FROM node:20.1-alpine3.16

WORKDIR /app

COPY env.sample package-lock.json package.json tsconfig.json VERSION .
RUN npm install

COPY dist/ /app/dist
COPY entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]
CMD npm run start:prod
