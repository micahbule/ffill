#!/bin/sh

SOURCE='env.sample'
DEST='.env'

cp $SOURCE $DEST
for i in AUTH0_ISSUER_URL AUTH0_AUDIENCE SECURITY_TOKEN MIKRO_ORM_HOST MIKRO_ORM_USER MIKRO_ORM_PASSWORD AUTH0_CLIENT_ID AUTH0_CLIENT_SECRET; do
    sed -i'' "s+$i=.*$+$i=$(printenv $i)+g" $DEST
done

exec "$@"
