import { wrap } from '@mikro-orm/core';
import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable, map } from 'rxjs';

@Injectable()
export class EntityTransformInterceptor<T> implements NestInterceptor {
  intercept(
    context: ExecutionContext,
    next: CallHandler<T>,
  ): Observable<any> | Promise<Observable<any>> {
    return next.handle().pipe(
      map((data: T) => {
        let serialized;

        if (Array.isArray(data)) {
          serialized = data.map((item) => wrap<T>(item).toJSON());
        } else {
          serialized = wrap<T>(data).toJSON();
        }

        return serialized;
      }),
    );
  }
}
