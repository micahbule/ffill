import { Entity, OneToOne, Property } from '@mikro-orm/core';
import { BaseEntity } from '../../base.entity';
import { User } from '../../user/entities/user.entity';

@Entity()
export class Client extends BaseEntity {
  @OneToOne({ primary: true, unique: true })
  user!: User;

  @Property()
  relocation_questionnaire_submitted = false;

  @Property({ nullable: true })
  date_of_last_rq_submission?: Date;
}
