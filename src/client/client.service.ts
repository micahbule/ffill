import { InjectRepository } from '@mikro-orm/nestjs';
import { Injectable } from '@nestjs/common';
import { Client } from './entities/client.entity';
import { EntityRepository } from '@mikro-orm/postgresql';
import { User } from 'src/user/entities/user.entity';

@Injectable()
export class ClientService {
  constructor(
    @InjectRepository(Client)
    private readonly clientRepository: EntityRepository<Client>,
  ) {}

  async createClientFromUser(user: User) {
    const newClient = new Client();

    newClient.user = user;

    await this.clientRepository.upsert(newClient);
    await this.clientRepository.flush();

    return newClient;
  }
}
