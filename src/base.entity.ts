import { Entity, Filter, PrimaryKey, Property } from '@mikro-orm/core';

@Entity({ abstract: true })
@Filter({ name: 'active', cond: { deleted_at: null } })
export abstract class BaseEntity {
  @PrimaryKey({ autoincrement: true })
  id!: number;

  @Property()
  created_at = new Date();

  @Property({ onUpdate: () => new Date() })
  updated_at = new Date();

  @Property({ nullable: true })
  deleted_at?: Date;
}
