import { Options } from '@mikro-orm/core';
import { BaseEntity } from './base.entity';
import { User } from './user/entities/user.entity';
import { Logger } from '@nestjs/common';
import { Client } from './client/entities/client.entity';

const logger = new Logger('MikroORM');
const config: Options = {
  entities: [BaseEntity, User, Client],
  dbName: 'liff',
  type: 'postgresql',
  debug: true,
  logger: logger.log.bind(logger),
};

export default config;
