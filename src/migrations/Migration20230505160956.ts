import { Migration } from '@mikro-orm/migrations';

export class Migration20230505160956 extends Migration {
  async up(): Promise<void> {
    this.addSql('alter table "client" drop constraint "client_pkey";');
    this.addSql(
      'alter table "client" add constraint "client_pkey" primary key ("id", "user_id");',
    );
  }

  async down(): Promise<void> {
    this.addSql('alter table "client" drop constraint "client_pkey";');
    this.addSql(
      'alter table "client" add constraint "client_pkey" primary key ("id");',
    );
  }
}
