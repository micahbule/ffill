import { Migration } from '@mikro-orm/migrations';

export class Migration20230505155505 extends Migration {
  async up(): Promise<void> {
    this.addSql(
      'create table "client" ("id" serial primary key, "created_at" varchar(255) not null, "updated_at" varchar(255) not null, "deleted_at" timestamptz(0) null, "user_id" int not null, "relocation_questionnaire_submitted" varchar(255) not null default false, "date_of_last_rq_submission" timestamptz(0) null);',
    );
    this.addSql(
      'alter table "client" add constraint "client_user_id_unique" unique ("user_id");',
    );

    this.addSql(
      'alter table "client" add constraint "client_user_id_foreign" foreign key ("user_id") references "user" ("id") on update cascade;',
    );
  }

  async down(): Promise<void> {
    this.addSql('drop table if exists "client" cascade;');
  }
}
