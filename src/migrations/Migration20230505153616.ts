import { Migration } from '@mikro-orm/migrations';

export class Migration20230505153616 extends Migration {
  async up(): Promise<void> {
    this.addSql(
      'create table "user" ("id" serial primary key, "created_at" varchar(255) not null, "updated_at" varchar(255) not null, "deleted_at" timestamptz(0) null, "email_address" varchar(255) not null, "first_name" varchar(255) null, "last_name" varchar(255) null, "auth0_user_id" varchar(255) not null);',
    );
    this.addSql(
      'alter table "user" add constraint "user_email_address_unique" unique ("email_address");',
    );
  }
}
