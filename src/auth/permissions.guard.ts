import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';

@Injectable()
export class PermissionsGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    let routePermissions =
      this.reflector.get<string[]>('permissions', context.getHandler()) || [];

    const userPermissions: string[] = context.getArgs()[0].user.permissions;
    const filterAnyPermissions = userPermissions.filter((userPermission) =>
      userPermission.includes(':any'),
    );

    if (filterAnyPermissions.length > 0) {
      filterAnyPermissions.forEach((filterAnyPermission) => {
        const anyPermissionPrefix = filterAnyPermission.substring(
          0,
          filterAnyPermission.indexOf(':'),
        );
        routePermissions = routePermissions.filter(
          (routePermission) => !routePermission.includes(anyPermissionPrefix),
        );
      });
    }

    if (routePermissions.length == 0) {
      return true;
    }

    const hasPermission = () =>
      routePermissions.every((routePermission) =>
        userPermissions.includes(routePermission),
      );

    return hasPermission();
  }
}
