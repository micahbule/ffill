import {
  CallHandler,
  ExecutionContext,
  Injectable,
  NestInterceptor,
  UnauthorizedException,
} from '@nestjs/common';
import { Observable, tap } from 'rxjs';
import { User } from 'src/user/entities/user.entity';

@Injectable()
export class UserOwnershipInterceptor implements NestInterceptor {
  intercept(
    context: ExecutionContext,
    next: CallHandler<any>,
  ): Observable<any> | Promise<Observable<any>> {
    const req = context.switchToHttp().getRequest();
    const user = req.user;

    return next.handle().pipe(
      tap((data: User) => {
        if (
          data.auth0_user_id !== user.profile.auth0_user_id &&
          !user.permissions.includes('read:any')
        )
          throw new UnauthorizedException('You cannot access this resource');
      }),
    );
  }
}
