import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  UseInterceptors,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { S2SGuard } from '../auth/s2s.guard';
import { AuthGuard } from '@nestjs/passport';
import { Permissions } from '../auth/permissions.decorator';
import { PermissionsGuard } from '../auth/permissions.guard';
import { UserOwnershipInterceptor } from '../auth/user_ownership.interceptor';
import { EntityTransformInterceptor } from '../utils/entity_transform.interceptor';
import { User } from './entities/user.entity';
import { ClientService } from '../client/client.service';

@Controller('user')
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly clientService: ClientService,
  ) {}

  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Get()
  @Permissions('read:users')
  @UseInterceptors(EntityTransformInterceptor<User>)
  findAll() {
    return this.userService.findAll();
  }

  @Post('/fromAuth0')
  @UseGuards(S2SGuard)
  async createFromAuth0(@Body() createUserDto: CreateUserDto) {
    const user = await this.userService.create(createUserDto);

    await this.clientService.createClientFromUser(user);

    return user;
  }

  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Get(':id')
  @Permissions('read:user')
  @UseInterceptors(UserOwnershipInterceptor, EntityTransformInterceptor<User>)
  findOne(@Param('id') id: string) {
    return this.userService.findOne(+id);
  }

  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Patch(':id')
  @Permissions('update:user')
  @UseInterceptors(UserOwnershipInterceptor, EntityTransformInterceptor<User>)
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update(+id, updateUserDto);
  }

  @UseGuards(AuthGuard('jwt'), PermissionsGuard)
  @Delete(':id')
  @Permissions('delete:user')
  @HttpCode(HttpStatus.NO_CONTENT)
  async remove(@Param('id') id: string) {
    await this.userService.remove(+id);
  }
}
