export class CreateUserDto {
  email_address: string;
  first_name: string;
  last_name: string;
  auth0_user_id: string;
}
