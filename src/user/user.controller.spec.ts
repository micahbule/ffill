import { Test, TestingModule } from '@nestjs/testing';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import { getRepositoryToken } from '@mikro-orm/nestjs';
import { User } from './entities/user.entity';
import { ConfigService } from '@nestjs/config';
import { ClientService } from '../client/client.service';

describe('UserController', () => {
  let controller: UserController;
  class MockClass {}

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        UserService,
        { provide: getRepositoryToken(User), useFactory: jest.fn() },
        { provide: ClientService, useClass: MockClass },
        ConfigService,
      ],
    }).compile();

    controller = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
