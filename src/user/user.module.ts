import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { ConfigService } from '@nestjs/config';
import { MikroOrmModule } from '@mikro-orm/nestjs';
import { User } from './entities/user.entity';
import { ClientModule } from 'src/client/client.module';

@Module({
  imports: [MikroOrmModule.forFeature([User]), ClientModule],
  controllers: [UserController],
  providers: [UserService, ConfigService],
})
export class UserModule {}
