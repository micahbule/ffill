import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { EntityRepository } from '@mikro-orm/postgresql';
import { InjectRepository } from '@mikro-orm/nestjs';
import { FilterQuery } from '@mikro-orm/core';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: EntityRepository<User>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    const newUser = new User();
    newUser.email_address = createUserDto.email_address;
    newUser.first_name = createUserDto.first_name;
    newUser.last_name = createUserDto.last_name;
    newUser.auth0_user_id = createUserDto.auth0_user_id;

    await this.userRepository.upsert(newUser);
    await this.userRepository.flush();

    return newUser;
  }

  async findAll(where: FilterQuery<User> = {}) {
    return this.userRepository.find(where, { filters: ['active'] });
  }

  async findOne(id: number, where: FilterQuery<User> = {}) {
    return this.userRepository.findOne(Object.assign({}, where, { id }), {
      filters: ['active'],
    });
  }

  findByAuth0Id(auth0Id: string) {
    return this.userRepository.findOne(
      { auth0_user_id: auth0Id },
      { filters: ['active'] },
    );
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.userRepository.findOne({ id });

    Object.keys(updateUserDto).forEach((key) => {
      if (!!updateUserDto[key]) {
        user[key] = updateUserDto[key];
      }
    });

    await this.userRepository.flush();

    return user;
  }

  async remove(id: number) {
    const user = await this.userRepository.findOne({ id });
    user.deleted_at = new Date();
    await this.userRepository.flush();
  }
}
