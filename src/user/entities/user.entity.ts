import { Entity, Property, Unique } from '@mikro-orm/core';
import { BaseEntity } from '../../base.entity';

@Entity()
export class User extends BaseEntity {
  @Property()
  @Unique()
  email_address!: string;

  @Property({ nullable: true })
  first_name?: string;

  @Property({ nullable: true })
  last_name?: string;

  @Property()
  auth0_user_id!: string;

  @Property({ name: 'onboarded' })
  getOnboarded() {
    return !!this.first_name && !!this.last_name;
  }
}
