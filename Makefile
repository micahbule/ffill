VERSION = $(shell cat VERSION)

build:
	npm install
	npm run build
	BUILDKIT_PROGRESS=plain docker build -t liff-api:${VERSION} .
